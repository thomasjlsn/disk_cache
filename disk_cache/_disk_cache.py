import atexit
import os
import pickle
import sqlite3
import sys
from functools import wraps
from hashlib import sha1
from time import time
from typing import Any, Callable, Optional, Union

import disk_cache._admin as admin
import disk_cache._ttl as ttl


def setup_db() -> str:
    cache_base = os.getenv('XDG_CACHE_HOME', os.path.expanduser('~/.cache'))
    program = os.path.realpath(os.path.abspath(sys.argv[0]))
    cache_dir = os.getenv(
        'PYTHON_CACHE_DIR',  # Allows user to choose their own cache location
        os.path.join(cache_base, 'python-disk-cache', program.replace(os.path.sep, '%'))
    )

    if not os.path.isdir(cache_dir):
        os.makedirs(cache_dir)

    return os.path.join(cache_dir, 'cache.db')


DB = sqlite3.connect(setup_db())

atexit.register(DB.close)
admin.run_admin_commands(DB)
atexit.register(admin.prune, DB)

# TODO I need to read more about these settings
#
# Docs: https://www.sqlite.org/pragma.html
# SQLite performance tuning: https://gist.github.com/phiresky/978d8e204f77feaa0ab5cca08d2d5b27
#
DB.execute(f'''PRAGMA mmap_size={2**27}''')   # 128M
DB.execute(f'''PRAGMA cache_size={2**15}''')  # 32K
DB.execute('''PRAGMA auto_vacuum=none''')
DB.execute('''PRAGMA synchronous=off''')
DB.execute('''PRAGMA journal_mode=wal''')
DB.execute('''PRAGMA temp_store=memory''')

DB.execute('''CREATE TABLE IF NOT EXISTS cache (hash TEXT PRIMARY KEY, obj BLOB, exp FLOAT)''')
DB.execute('''DELETE FROM cache WHERE (?) > exp''', (time(),))

DB.commit()


class Abort(Exception):
    '''Aborts @cache() and can be used to return a value without caching it.'''
    __slots__ = ['return_value']

    def __init__(self, return_value: Optional[Any] = None) -> None:
        self.return_value = return_value


def hash_function_signature(func: Callable, args: tuple, kwargs: dict, per_directory: Union[bool, int]) -> str:
    '''Use information about func to generate a unique hash.'''
    identifying_attributes = (
        func.__name__,                        # What is func's name?
        func.__code__.co_code,                # What is it's bytecode?
        *map(repr, (*args, *kwargs)),         # What arguments was it given?
        os.getcwd() if per_directory else ''  # Where is it running?
    )

    hash = sha1()

    for attr in map(str, identifying_attributes):
        hash.update(attr.encode())

    return hash.hexdigest()


def cache(ttl: int = ttl.hour(), per_directory: Union[bool, int] = False, verbose: Union[bool, int] = False, guid: str = None) -> Callable:
    '''A decorator that pickles a function's result and stores it in a sqlite3 databse.

    Arguments:
        ttl              time in seconds that the cache entry should live
        per_directory    cache results per directory
        verbose          write verbose output to stderr
    '''
    def decorator(func: Callable) -> Callable:
        return make_cache_wrapper(func, ttl, per_directory, verbose, guid)

    return decorator


def make_cache_wrapper(func: Callable, ttl: int, per_directory: Union[bool, int], verbose: Union[bool, int], guid: str = None) -> Callable:
    '''Create a decorator that wraps func.'''
    def verbose_output(msg: str) -> None:
        if verbose and sys.stderr.isatty():
            sys.stderr.write(f'disk cache: {func.__name__}() -> {msg}\n')

    @wraps(func)
    def cached_function(*args: tuple, **kwargs: dict) -> Any:
        hash = guid or hash_function_signature(func, args, kwargs, per_directory)

        query = DB.execute('''SELECT obj, exp FROM cache WHERE hash = (?) ''', (hash,))

        if (result := query.fetchone()):
            obj, expires = result

            if expires and (time() > expires):
                verbose_output('MISS (expired)')
                DB.execute('''DELETE FROM cache WHERE hash = (?)''', (hash,))
                DB.commit()
            else:
                verbose_output('HIT')
                return pickle.loads(obj)
        else:
            verbose_output('MISS')

        try:
            returned_value = func(*args, **kwargs)
        except Abort as abort:
            verbose_output('ABORT')
            return abort.return_value

        expires = time() + ttl

        DB.execute('''INSERT OR IGNORE INTO cache VALUES(?, ?, ?)''',
                   (hash, pickle.dumps(returned_value, -1), expires))
        DB.commit()

        return returned_value

    return cached_function
