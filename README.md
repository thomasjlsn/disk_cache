# disk cache

## Usage

Basic usage:
```
>>> import disk_cache as dc
>>> @dc.cache()
    def my_func(n):
        return expensive_compution(n)
```

Return from a function without caching the result:
```
>>> import disk_cache as dc
>>> @dc.cache()
    def my_func(lst):
        result = []
        for i in n:
            if not i:
                raise dc.Abort(result)
            results.append(i)
        return result
>>> my_func([True, True, False])
[True, True]
```

Calculating ttl:
```
>>> from disk_cache import ttl
>>> ttl.month()  # one month
2592000
>>> ttl.week(2)  # two weeks
1209600
```
