import sys
from time import time


def clear(db) -> None:
    '''Remove all cache entries.'''
    db.execute('''DELETE FROM cache''')
    db.commit()


def prune(db) -> None:
    '''Remove expired cache entries.'''
    db.execute('''DELETE FROM cache WHERE (?) > exp''', (time(),))
    db.commit()


def vacuum(db) -> None:
    '''Vacuum the database.'''
    db.execute('''VACUUM''')
    db.commit()


def run_admin_commands(db):
    '''Adds flags to the importing program to run administrative commands.'''
    admin_commands = {
        # XXX I'm not set on these flag names. I just wanted to pick names
        #     that would be unlikely to conflict with existing flags the
        #     importing program may define.
        '--dc-clear':  clear,
        '--dc-prune':  prune,
        '--dc-vacuum': vacuum,
    }

    commands_called = 0

    for arg in sys.argv[1:]:
        if arg in admin_commands:
            admin_commands[arg](db)
            commands_called += 1

    if commands_called:
        sys.exit(0)
