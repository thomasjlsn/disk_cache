'''Functions that calculate ttl duration in a human-readable way.'''


def inf() -> int:
    return 0


def minute(n: int = 1) -> int:
    return n * 60


def hour(n: int = 1) -> int:
    return n * 3600


def day(n: int = 1) -> int:
    return n * 86400


def week(n: int = 1) -> int:
    return n * 604800


def month(n: int = 1) -> int:
    # A month is calculated as 30.4166 days (365/12)
    return n * 2628000


def year(n: int = 1) -> int:
    return n * 31536000
