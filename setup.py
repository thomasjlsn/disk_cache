#!/usr/bin/env python3

from setuptools import setup

setup(
    name='disk_cache',
    version='0.0.0',
    description='Cache a function\'s result locally on disk',
    long_description='Check out the README on [GitLab](https://gitlab.com/thomasjlsn/disk_cache)!',
    long_description_content_type='text/markdown',
    author='Thomas Ellison',
    author_email='thomasjlsn@gmail.com',
    url='https://gitlab.com/thomasjlsn/disk_cache',
    scripts=['cached'],
    packages=['disk_cache'],
    classifiers=[
        'Programming Language :: Python :: 3',
        'Development Status :: 3 - Alpha',
    ]
)
